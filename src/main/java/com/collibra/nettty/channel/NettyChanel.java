package com.collibra.nettty.channel;

import com.collibra.nettty.server.ServerMessage;
import com.collibra.nettty.session.Session;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyChanel implements MainChannel {

    private Channel channel;
    private Session session;
    private Logger log ;

    public NettyChanel(Channel channel, Session session) {
        this.channel = channel;
        this.session = session;
        this.log = LoggerFactory.getLogger(NettyChanel.class);
    }

    public Session getSession() {
        return this.session;
    }

    @Override
    public Channel getChannel() {
        return this.channel;
    }

    public void close() {
        this.log.info("close");
        this.channel.close();
    }

    public void write(ServerMessage serverMessage) {
        this.channel.writeAndFlush(serverMessage.getMessage());
    }
}
