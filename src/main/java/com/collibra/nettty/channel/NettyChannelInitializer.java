package com.collibra.nettty.channel;

import com.collibra.nettty.ChatServerHandler;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.*;
import com.collibra.nettty.graphListener.*;
import com.collibra.nettty.messageListener.ClientEntryListener;
import com.collibra.nettty.messageListener.ExitListener;
import com.collibra.nettty.messageListener.GreetingListener;
import com.collibra.nettty.messageListener.UnknownHandler;
import com.collibra.nettty.session.SessionFactory;
import com.collibra.nettty.session.ClientNameRepo;
import com.collibra.nettty.session.Session;
import com.collibra.nettty.session.SessionNameBinding;
import com.collibra.util.ServerConstant;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.util.CharsetUtil;

import java.util.HashSet;
import java.util.Set;

@ChannelHandler.Sharable
public class NettyChannelInitializer extends ChannelInitializer {

    Session session;
    ClientNameRepo inMemoryNameRepository;
    SearchPath routeFinder;
    CloserThanThisNode closerThanService;
    Set<NewMessageListener> newMessageListeners;
    Set<NewChannelListener> newChannelListeners;
    GraphStructureImpl graphStructure;

    protected void createHandlers() {
        this.session =  new SessionNameBinding().session();
        this.graphStructure = new GraphStructureImpl();
        this.inMemoryNameRepository = new SessionFactory();
        inMemoryNameRepository.setName(session,session.getUuid());

        this.routeFinder = new DijkstraShortestPath();
        this.closerThanService = new CloserThanServiceImpl();

        this.newMessageListeners = new HashSet<>();

        this.newChannelListeners = new HashSet<>();
        GreetingListener greetingListener = new GreetingListener();
        newChannelListeners.add(greetingListener);


        newMessageListeners.add(new ExitListener(inMemoryNameRepository));
        newMessageListeners.add(new ClientEntryListener(inMemoryNameRepository));
        newMessageListeners.add(new UnknownHandler());

        newMessageListeners.add(new AddNodeListener(graphStructure));
        newMessageListeners.add(new RemoveNodeListener(graphStructure));
        newMessageListeners.add(new AddEdgeListener(graphStructure));
        newMessageListeners.add(new RemoveEdgeListener(graphStructure));
        newMessageListeners.add(new ShortestPathListener(graphStructure, routeFinder));
        newMessageListeners.add(new ClosestNodeListener(graphStructure, closerThanService));
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        createHandlers();
        ChannelPipeline p = ch.pipeline();
        p.addLast(new ReadTimeoutHandler(ServerConstant.WAITING_TIME));
        p.addLast(new StringDecoder(CharsetUtil.UTF_8));
        p.addLast(new StringEncoder(CharsetUtil.UTF_8));
        p.addLast(new ChatServerHandler(this.session, this.inMemoryNameRepository, this.graphStructure,
                this.routeFinder, this.closerThanService, this.newMessageListeners, this.newChannelListeners));


    }
}
