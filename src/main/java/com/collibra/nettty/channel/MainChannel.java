package com.collibra.nettty.channel;

import com.collibra.nettty.server.ServerMessage;
import com.collibra.nettty.session.Session;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;

@ChannelHandler.Sharable
public interface MainChannel extends AutoCloseable {

    Session getSession();
    Channel getChannel();
    void write(ServerMessage var1);

}
