package com.collibra.nettty.channel;

public interface NewChannelListener {
     void onNewChannel(MainChannel channel);
}
