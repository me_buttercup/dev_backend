package com.collibra.nettty;

public final class ClientMessage {

    private final String message;

    public ClientMessage(String message) {
        this.message = message.trim();
    }
    public final String getMessage() {
        return this.message;
    }

}
