package com.collibra.nettty.graph;

import java.util.LinkedList;

public final class Node {

    private final String name;
    private boolean visited;
    private final LinkedList<Edge> edges;

    public Node(String name) {
        this.visited = false;
        this.edges = new LinkedList<>();
        this.name = name;
    }

    public boolean equals(Object node) {
        if (node instanceof Node) {
            Node otherNode = (Node)node;
            if (this.name.equals(otherNode.name) &&
                    this.name.equals(otherNode.name)) {
                return true;
            }else {return  false;}
        }else {
            return false;
        }
    }
    public String getName() {
        return name;
    }

    public LinkedList<Edge> getEdges() {
        return edges;
    }

    public boolean isVisited() {
        return visited;
    }

    public void visit() {
        visited = true;
    }

    public void unvisit() {
        visited = false;
    }

}
