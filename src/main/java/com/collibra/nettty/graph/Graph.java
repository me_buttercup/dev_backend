package com.collibra.nettty.graph;

import java.util.Optional;
import java.util.Set;

public final class Graph {

    private final Set<Node> nodes;

    Graph(Set<Node> nodes){
        this.nodes = nodes;
    }

    public Set<Node> getGraph() {
        return this.nodes;
    }

    public Graph addNode(String nodeName) {
        if(isExist(nodeName) != null) {
            throw new RuntimeException("NODE EXIST");
        }
        Node node =  new Node(nodeName);
        this.nodes.add(node);
        return new Graph(this.nodes);
    }

    public Graph removeNode(String nodeName) {
        Node nodeToRemove = isExist(nodeName);
        if(nodeToRemove == null) {
            throw new RuntimeException("ERROR: NODE NOT FOUND");
        }

        this.nodes.remove(nodeToRemove);
        this.nodes.stream().forEach(node-> {
            Optional<Edge> edges = node.getEdges().stream().filter (
                edge -> edge.destination.getName().equals(nodeName)
            ).findAny();
            if(edges != null){
                node.getEdges().remove(edges);
                this.nodes.add(node);
            }
        });
        return new Graph(this.nodes);
    }

    public Graph addEdge(String sLable, String dLable, int weight) {
        Node sourceNode = isExist(sLable);
        Node destinationNode = isExist(dLable);

        if(sourceNode == null)
            throw new RuntimeException("ERROR: NODE NOT FOUND");

        if( destinationNode == null)
            throw new RuntimeException("ERROR: NODE NOT FOUND");

        this.nodes.stream()
            .filter(n->n.getName().equals(sourceNode.getName()))
            .forEach(source-> {
                Edge we = isEdgeExist(source, destinationNode);
                if(we != null) {
                    if(we.weight != weight) {
//                        if(isWeightDifferent(source, destinationNode, weight)) {
                        //source.edges.remove(we);
                        //we.weight = weight;
                        //source.edges.add(we);
                        source.getEdges().add(new Edge(sourceNode, destinationNode, weight));
                        this.nodes.add(source);

                    }
                } else {
                    source.getEdges().add(new Edge(sourceNode, destinationNode, weight));
                    this.nodes.add(source);
                }
            });
        return new Graph(this.nodes);
    }

    private Node isExist(String nodeName) {
        return this.nodes.stream()
                .filter(n->n.getName().equals(nodeName)).findAny().orElse(null);
    }

    private Edge isEdgeExist(Node source, Node b) {
        return source.getEdges().stream().filter(edge->
                edge.destination.getName().equals(b.getName()))
                .findFirst()
                .orElse(null);
    }


    public Graph removeEdge(String sLable, String dLable) {
        Node sourceNode = isExist(sLable);
        Node destinationNode = isExist(dLable);

        if(sourceNode == null || destinationNode == null) {
            throw new RuntimeException("ERROR: NODE NOT FOUND");
        }

        this.nodes.stream().forEach(node-> {
            Optional<Edge> edgesToRemove = node.getEdges().stream()
                    .filter(edge -> edge.destination.getName().equals(destinationNode.getName()) &&
                            edge.source.getName().equals(sourceNode.getName()))
                    .findAny();
            if(edgesToRemove !=null ){
                node.getEdges().remove(edgesToRemove);
                this.nodes.add(node);
            }
        });

        return new Graph(this.nodes);
    }
}
