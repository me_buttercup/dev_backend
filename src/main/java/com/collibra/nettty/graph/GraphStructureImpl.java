package com.collibra.nettty.graph;

import io.netty.channel.ChannelHandler;

import java.util.HashSet;

@ChannelHandler.Sharable
public class GraphStructureImpl implements GraphStructure {

    private Graph graph = new Graph(new HashSet<>());

    @Override
    public void addNode(String node) {
        this.graph = graph.addNode(node);
    }

    @Override
    public void addEdge(String source, String destination, int weight) {
        this.graph = graph.addEdge(source, destination, weight);
    }

    @Override
    public void removeNode(String node) {
        this.graph = graph.removeNode(node);
    }

    @Override
    public void removeEdge(String source, String destination) {
        this.graph = graph.removeEdge(source, destination);
    }

    @Override
    public Graph getReadOnlyGraph() {
        return this.graph;
    }
}
