package com.collibra.nettty.graph;

public interface GraphStructure {

    void addNode(String node);

    void addEdge(String source, String destination, int weight);

    void removeNode(String node);

    void removeEdge(String source , String destination);

    Graph getReadOnlyGraph();
}
