package com.collibra.nettty.graph;

public interface CloserThanThisNode {
    String closerThan(Graph graph, String sLabel, int weight);
}
