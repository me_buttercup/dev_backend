package com.collibra.nettty.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class CloserThanServiceImpl implements CloserThanThisNode {

    Logger log = LoggerFactory.getLogger(CloserThanServiceImpl.class);

    @Override
    public String closerThan(Graph graph, String sLabel, int totalweight) {
        Node node = graph.getGraph().stream().filter(n -> n.getName().equals(sLabel))
                .findAny().orElse(null);

        if(node == null) {
            log.trace("exiting due to node not found error");
            return "ERROR: NODE NOT FOUND";
        }

        HashMap<Node, Integer> distances = new HashMap();
        distances.put(node, 0);
        HashSet toVisitNodes = new HashSet();
        toVisitNodes.add(node);
        HashSet<Node> visited = new HashSet();

        while(!toVisitNodes.isEmpty()) {
            Node current = removeFirstNode(toVisitNodes);
            visited.add(current);
            int currentDistance = distances.get(current);
            for(Edge edge :current.getEdges()) {
                Node neighbour = edge.destination;
                int cost = getShortesWeight(current, neighbour);
                int neighbourDistance = Integer.MAX_VALUE;
                try {
                    neighbourDistance = distances.get(edge.destination);
                } catch (Exception e){
                }
                //if there are more than one edges we need to get the min. val. so get the shortest path
                //instead of edge weight
                int newDistance = currentDistance + cost;
                if (neighbourDistance > newDistance) {
                    distances.put(neighbour, newDistance);
                    toVisitNodes.add(neighbour);
                } else if (!visited.contains(neighbour)) {
                    toVisitNodes.add(neighbour);
                }
            }
        }

        return distances.keySet().stream()
                .filter(key->
                distances.get(key) < totalweight && !key.getName().equals(node.getName()))
                .map(s->s.getName()).sorted().collect(Collectors.joining(","));
    }

    private int getShortesWeight(Node current, Node neighbour) {
        ArrayList<Integer> weights = new ArrayList<>();
        for(Edge edge : current.getEdges()) {
            if(edge.destination.getName().equals(neighbour.getName()) &&
               edge.source.getName().equals(current.getName())){
                weights.add(edge.weight);
            }
        }

        Collections.sort(weights);
        return weights.get(0);
    }

    private Node removeFirstNode(Set nodes) {
        Iterator iterator = nodes.iterator();
        Node node = (Node)iterator.next();
        iterator.remove();
        return node;
    }


}
