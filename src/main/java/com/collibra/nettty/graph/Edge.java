package com.collibra.nettty.graph;

public final class Edge implements Comparable<Edge> {

    Node source;
    Node destination;
    int weight;

    public Edge(Node source, Node destination, int weight){
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        Node edge = this.source;
        int obj = (edge != null ? edge.hashCode() : 0) * 31;
        return (obj + (this.destination != null ? this.destination.hashCode() : 0)) * 31 + this.weight;
    }

    @Override
    public boolean equals(Object edge) {
        if (this != edge) {
            if (edge instanceof Edge) {
                Edge var2 = (Edge)edge;
                if (this.source.equals(var2.source) &&
                        this.destination.equals(var2.destination) && this.weight == var2.weight) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int compareTo(Edge edgeOther) {
        return (int)(this.weight - edgeOther.weight);
    }

}
