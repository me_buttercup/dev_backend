package com.collibra.nettty.graph;

public interface SearchPath {
    int DijkstraShortestPath(Graph graph, String sNode , String dNode);
}
