package com.collibra.nettty.graph;

import java.util.HashMap;
import java.util.Set;

public class DijkstraShortestPath implements SearchPath {

    public int DijkstraShortestPath(Graph graph , String sLabel, String dLabel) {

        if(sLabel.equals(dLabel)) {
            //same node
            return 0;
        }
        Node start = graph.getGraph().stream().filter(node->node.getName().equals(sLabel)).findAny().orElse(null);
        Node end = graph.getGraph().stream().filter(node->node.getName().equals(dLabel)).findAny().orElse(null);

        if(start == null  || end == null) {
            //node doesnt exist
            return -1;
        }

        HashMap<Node, Node> changedAt = new HashMap<>();
        changedAt.put(start, null);
        HashMap<Node, Integer> shortestPathMap = new HashMap<>();

        for (Node node : graph.getGraph()) {
            if (node == start)
                shortestPathMap.put(start, 0);
            else shortestPathMap.put(node, Integer.MAX_VALUE);
        }
        for (Edge edge : start.getEdges()) {
            shortestPathMap.put(edge.destination, edge.weight);
            changedAt.put(edge.destination, start);
        }

        start.visit();
        while (true) {
            Node currentNode = closestReachableUnvisited(shortestPathMap, graph.getGraph());
            if (currentNode == null) {
                //"There is no path"
                resetNodesVisited(graph.getGraph());
                return Integer.MAX_VALUE;
            }
            if (currentNode == end) {
                Node child = end;
                String path = end.getName();
                while (true) {
                    Node parent = changedAt.get(child);
                    if (parent == null) {
                        break;
                    }
                    path = parent.getName() + " " + path;
                    child = parent;
                }
                int cost = shortestPathMap.get(end);
                resetNodesVisited(graph.getGraph());
                return cost;
            }
            currentNode.visit();
            for (Edge edge : currentNode.getEdges()) {
                if (edge.destination.isVisited())
                    continue;

                if (shortestPathMap.get(currentNode)
                        + edge.weight
                        < shortestPathMap.get(edge.destination)) {
                    shortestPathMap.put(edge.destination,
                            shortestPathMap.get(currentNode) + edge.weight);
                    changedAt.put(edge.destination, currentNode);
                }
            }
        }
    }

    public void resetNodesVisited(Set<Node> nodes) {
        for (Node node : nodes) {
            node.unvisit();
        }
    }

    private Node closestReachableUnvisited(HashMap<Node, Integer> shortestPathMap, Set<Node> nodes) {
        double shortestDistance = Integer.MAX_VALUE;
        Node closestReachableNode = null;
        for (Node node : nodes) {
            if (node.isVisited())
                continue;

            double currentDistance = shortestPathMap.get(node);
            if (currentDistance == Integer.MAX_VALUE)
                continue;

            if (currentDistance < shortestDistance) {
                shortestDistance = currentDistance;
                closestReachableNode = node;
            }
        }
        return closestReachableNode;
    }


}
