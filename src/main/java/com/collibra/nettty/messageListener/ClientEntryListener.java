package com.collibra.nettty.messageListener;

import com.collibra.nettty.*;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.session.ClientNameRepo;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.nettty.session.Uuid;

public class ClientEntryListener implements NewMessageListener {

    private final String clientHiPrefix;
    private final ClientNameRepo nameRepository;

    public ClientEntryListener(ClientNameRepo nameRepository) {
        this.nameRepository = nameRepository;
        this.clientHiPrefix = "HI, I AM";
    }

    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage!=null && clientMessage.getMessage().startsWith(this.clientHiPrefix)){
            String name = clientMessage.getMessage().substring(clientHiPrefix.length());
            channel.write(new ServerMessage("HI" + name));
            this.nameRepository.setName(channel.getSession(), new Uuid(name));
        }
    }
}
