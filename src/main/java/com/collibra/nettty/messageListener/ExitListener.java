package com.collibra.nettty.messageListener;

import com.collibra.nettty.*;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.session.ClientNameRepo;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.nettty.session.Uuid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExitListener implements NewMessageListener, TimeoutListener {

    private final String byeMessage;
    private final ClientNameRepo clientNameRepo;
    Logger log = LoggerFactory.getLogger(ExitListener.class);

    public ExitListener(ClientNameRepo clientNameRepo) {
        this.clientNameRepo = clientNameRepo;
        this.byeMessage = "BYE MATE!";
    }

    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage.getMessage().equals(this.byeMessage)){
            log.trace("sending bye!");
            this.sendBye(channel);
        }
    }

    @Override
    public void timeout(MainChannel channel) {
        log.trace("timeout happened ");
        this.sendBye(channel);
    }

    private void sendBye(MainChannel channel) {
        Uuid name = this.clientNameRepo.getName(channel.getSession());
        long time = System.currentTimeMillis() - channel.getSession().getTimestamp();
        channel.write(new ServerMessage("BYE" + name.getValue() + ", WE SPOKE FOR " + time + " MS"));
    }
}
