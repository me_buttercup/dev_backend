package com.collibra.nettty.messageListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.server.ServerMessage;

import java.util.Arrays;

public class UnknownHandler  implements NewMessageListener {

    String[] prefix = {"HI", "BYE", "ADD", "REMOVE", "CLOSE", "SHORTEST"};
    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        String handledMassage = Arrays.asList(prefix).stream()
                .filter(prefix->clientMessage.getMessage().startsWith(prefix))
                .findAny()
                .orElse(null);
        if(handledMassage == null){
            channel.write(new ServerMessage("SORRY, I DID NOT UNDERSTAND THAT"));
        }
    }
}
