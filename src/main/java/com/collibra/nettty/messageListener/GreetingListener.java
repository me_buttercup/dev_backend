package com.collibra.nettty.messageListener;

import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.channel.NewChannelListener;
import com.collibra.nettty.server.ServerMessage;

public class GreetingListener implements NewChannelListener {

    public void onNewChannel(MainChannel channel) {
        ServerMessage serverMessage = new ServerMessage("HI, I AM " + this.uuid(channel));
        channel.write(serverMessage);
    }

    private String uuid(MainChannel channel) {
        return channel.getSession().getUuid().getValue();
    }
}
