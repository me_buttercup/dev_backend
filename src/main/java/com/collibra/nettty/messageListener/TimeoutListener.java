package com.collibra.nettty.messageListener;

import com.collibra.nettty.channel.MainChannel;

public interface TimeoutListener {
    void timeout(MainChannel channel);
}
