package com.collibra.nettty.graphListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.util.ServerConstant;

public class RemoveEdgeListener implements NewMessageListener {
    GraphStructure graphStructure;

    public RemoveEdgeListener(GraphStructure graphRepository) {
        this.graphStructure = graphRepository;
    }

    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage.getMessage().startsWith(ServerConstant.PHASE2_REMOVE_EDGE)) {
            String message = clientMessage.getMessage().substring(ServerConstant.PHASE2_REMOVE_EDGE.length()).trim();
            String split [] = message.split(" ");
            try {
                graphStructure.removeEdge(split[0], split[1]);
                channel.write(new ServerMessage("EDGE REMOVED"));
            } catch (RuntimeException re) {
                channel.write(new ServerMessage("ERROR: NODE NOT FOUND"));
            }
        }
    }
}
