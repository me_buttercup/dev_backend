package com.collibra.nettty.graphListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.util.ServerConstant;

public class AddNodeListener implements NewMessageListener {

    GraphStructure graphStructure;

    public AddNodeListener(GraphStructure graphRepository) {
        this.graphStructure = graphRepository;
    }

    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage.getMessage().startsWith(ServerConstant.PHASE2_ADD_NODE)) {
            String nodeName = clientMessage.getMessage().substring(ServerConstant.PHASE2_ADD_NODE.length()).trim();
            try {
                graphStructure.addNode(nodeName);
                channel.write(new ServerMessage("NODE ADDED"));
            } catch (RuntimeException re) {
                channel.write(new ServerMessage("ERROR: NODE ALREADY EXISTS"));
            }
        }
    }
}
