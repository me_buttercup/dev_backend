package com.collibra.nettty.graphListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.util.ServerConstant;

public class AddEdgeListener implements NewMessageListener {

    GraphStructure graphStructure;

    public AddEdgeListener(GraphStructure graphRepository) {
        this.graphStructure = graphRepository;
    }


    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage.getMessage().startsWith(ServerConstant.PHASE2_ADD_EDGE)) {
            String message = clientMessage.getMessage().substring(ServerConstant.PHASE2_ADD_EDGE.length()).trim();
            String split [] = message.split(" ");
            try {
                graphStructure.addEdge(split[0], split[1], Integer.parseInt(split[2]));
                channel.write(new ServerMessage("EDGE ADDED"));
            } catch (RuntimeException re) {
                channel.write(new ServerMessage("ERROR: NODE NOT FOUND"));
            }
        }
    }
}
