package com.collibra.nettty.graphListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.util.ServerConstant;

public class RemoveNodeListener implements NewMessageListener {

    GraphStructure graphStructure;

    public RemoveNodeListener(GraphStructure graphRepository) {
        this.graphStructure = graphRepository;
    }

        @Override
        public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
            if(clientMessage.getMessage().startsWith(ServerConstant.PHASE2_REMOVE_NODE)) {
                String nodeName = clientMessage.getMessage().substring(ServerConstant.PHASE2_REMOVE_NODE.length()).trim();
                try {
                    graphStructure.removeNode(nodeName);
                    channel.write(new ServerMessage("NODE REMOVED"));
                } catch (RuntimeException re) {
                    channel.write(new ServerMessage("ERROR: NODE NOT FOUND"));
                }
            }
        }

}
