package com.collibra.nettty.graphListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.Graph;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.graph.SearchPath;
import com.collibra.nettty.server.ServerMessage;
import com.collibra.util.ServerConstant;

public class ShortestPathListener implements NewMessageListener {

    GraphStructure graphStructure;
    SearchPath routeFinder;

    public ShortestPathListener(GraphStructure graphRepository, SearchPath routeFinder) {
        this.graphStructure = graphRepository;
        this.routeFinder = routeFinder;
    }

    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage.getMessage().startsWith(ServerConstant.PHASE3_PREFIX)) {
            String message = clientMessage.getMessage().substring(ServerConstant.PHASE3_PREFIX.length()).trim();
            String split [] = message.split(" ");
            try {
                Graph graph = graphStructure.getReadOnlyGraph();
                int cost = routeFinder.DijkstraShortestPath(graph, split[0], split[1]);
                if(cost>0) {
                   channel.write(new ServerMessage(cost));
                } else {
                    channel.write(new ServerMessage("ERROR: NODE NOT FOUND"));
                }
            } catch (RuntimeException re) {
                channel.write(new ServerMessage("ERROR: NODE NOT FOUND"));
            }
        }
    }
}
