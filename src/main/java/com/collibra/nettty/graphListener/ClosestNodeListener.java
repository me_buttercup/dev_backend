package com.collibra.nettty.graphListener;

import com.collibra.nettty.ClientMessage;
import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.NewMessageListener;
import com.collibra.nettty.graph.CloserThanThisNode;
import com.collibra.nettty.graph.Graph;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.server.ServerMessage;

import com.collibra.util.ServerConstant;

public class ClosestNodeListener implements NewMessageListener {

    GraphStructure graphStructure;
    CloserThanThisNode closerThanService;

    public ClosestNodeListener(GraphStructure graphRepository, CloserThanThisNode closerThanService) {
        this.graphStructure = graphRepository;
        this.closerThanService = closerThanService;
    }

    @Override
    public void onNewMessage(ClientMessage clientMessage, MainChannel channel) {
        if(clientMessage.getMessage().startsWith(ServerConstant.PHASE4_PREFIX)) {
            String message = clientMessage.getMessage().substring(ServerConstant.PHASE4_PREFIX.length()).trim();
            String split [] = message.split(" ");
            try {
                Graph graph = graphStructure.getReadOnlyGraph();
                String nodeList = closerThanService.closerThan(graph, split[1],Integer.parseInt( split[0]));
                channel.write(new ServerMessage(nodeList));
            } catch (RuntimeException re) {
                channel.write(new ServerMessage("ERROR: NODE NOT FOUND"));
            }
        }

    }
}
