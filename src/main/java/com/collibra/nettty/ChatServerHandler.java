package com.collibra.nettty;

import com.collibra.nettty.channel.MainChannel;
import com.collibra.nettty.channel.NettyChanel;
import com.collibra.nettty.channel.NewChannelListener;
import com.collibra.nettty.graph.CloserThanThisNode;
import com.collibra.nettty.graph.GraphStructure;
import com.collibra.nettty.graph.SearchPath;
import com.collibra.nettty.messageListener.*;
import com.collibra.nettty.session.ClientNameRepo;
import com.collibra.nettty.session.Session;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.ReadTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

@ChannelHandler.Sharable
public class ChatServerHandler extends SimpleChannelInboundHandler<String> {

    private MainChannel colibraChannel;
    private Logger log = LoggerFactory.getLogger(ChatServerHandler.class);

    private final Set<NewMessageListener> newMessageListeners;
    private final Set<NewChannelListener> newChannelListeners;

    private final TimeoutListener timeoutListener;
    private final Session session;

    public ChatServerHandler(Session session, ClientNameRepo inMemoryNameRepository,
                             GraphStructure graphRepository,
                             SearchPath routeFinder, CloserThanThisNode closerThanService,
                             Set<NewMessageListener> newMessageListeners, Set<NewChannelListener> newChannelListeners) {

        this.session = session;
        this.newMessageListeners = newMessageListeners;
        this.newChannelListeners = newChannelListeners;
        this.timeoutListener = new ExitListener(inMemoryNameRepository);
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx)  {
        this.colibraChannel = new NettyChanel(ctx.channel(), this.session);
        this.newChannelListeners.forEach(listener -> listener.onNewChannel(this.colibraChannel));
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String msg) {
        newMessageListeners.forEach(listener -> listener.onNewMessage(new ClientMessage(msg), this.colibraChannel));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        if (cause instanceof ReadTimeoutException) {
            this.timeoutListener.timeout(this.colibraChannel);
        } else {
            try {
                ctx.channel().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
