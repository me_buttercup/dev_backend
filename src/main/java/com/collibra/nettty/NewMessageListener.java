package com.collibra.nettty;

import com.collibra.nettty.channel.MainChannel;

public interface NewMessageListener {
    void onNewMessage(ClientMessage clientMessage, MainChannel mainChannel);
}
