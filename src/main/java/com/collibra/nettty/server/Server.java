package com.collibra.nettty.server;

import com.collibra.nettty.channel.NettyChannelInitializer;
import com.collibra.util.ServerConstant;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Server implements ServerInterface{

    private EventLoopGroup bossGroup = new NioEventLoopGroup(6);
    private EventLoopGroup workerGroup = new NioEventLoopGroup(25);
    private ChannelFuture channelFuture;

    public static void main(String[] args)  {
        Server server = new Server();
        server.execute();
    }

    private void execute()  {
        try {
            ServerBootstrap b = new ServerBootstrap();
            channelFuture = b.group(bossGroup, workerGroup)
            .channel(NioServerSocketChannel.class)
            .option(ChannelOption.SO_BACKLOG, 128)
            .childOption(ChannelOption.SO_KEEPALIVE, true)
              .childHandler(new NettyChannelInitializer())
            .bind(ServerConstant.PORT).sync();
            System.out.println("Chat Server started. Ready to accept chat clients.");
        } catch (InterruptedException ie) {
        }
    }

    @Override
    public void close() throws Exception {
        channelFuture.channel().closeFuture().sync();
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}
