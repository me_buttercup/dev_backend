package com.collibra.nettty.server;

public final class ServerMessage {

    private final String message;

    public final String getMessage() {
        return this.message;
    }

    public ServerMessage(String message) {
//        super();
        this.message = message+"\n";
    }

    public ServerMessage(Object message) {
        this(message.toString());
    }

    public final ServerMessage copy(String message){
        return new ServerMessage(message);
    }

}
