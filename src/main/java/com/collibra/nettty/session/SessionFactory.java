package com.collibra.nettty.session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SessionFactory implements ClientNameRepo {

    private final ConcurrentHashMap map = new ConcurrentHashMap();

    @Override
    public void setName(Session session, Uuid name) {
        ((Map)this.map).put(session, name);
    }

    @Override
    public Uuid getName(Session session) {
        Uuid name = (Uuid)this.map.get(session);
        if (name != null) {
            return name;
        } else {
            throw new RuntimeException("There is no name for session " + session + '!');
        }
    }
}
