package com.collibra.nettty.session;


import java.util.UUID;

public class SessionNameBinding {
    public Uuid uuid() {
        String name = UUID.randomUUID().toString();
        return new Uuid(name);
    }

    public Session session() {
        long startTime = System.currentTimeMillis();
        String name = UUID.randomUUID().toString();
        return new Session(startTime, new Uuid(name));
    }

}


