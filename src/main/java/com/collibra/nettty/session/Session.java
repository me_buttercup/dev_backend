package com.collibra.nettty.session;

public class Session {

    private final long timestamp;
    private final Uuid uuid;

    public final long getTimestamp() {
        return this.timestamp;
    }

    public final Uuid getUuid() {
        return this.uuid;
    }

    public Session(long timestamp,Uuid uuid) {
        super();
        this.timestamp = timestamp;
        this.uuid = uuid;
    }


}
