package com.collibra.nettty.session;

public interface ClientNameRepo {
    void setName(Session session, Uuid name);
    Uuid getName( Session session);
}
