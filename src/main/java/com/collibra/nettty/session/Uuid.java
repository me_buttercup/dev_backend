package com.collibra.nettty.session;


public final class Uuid {

    private final String value;
    public final String getValue() {
        return this.value;
    }
    public Uuid(String value) {
        super();
        this.value = value;
    }
}