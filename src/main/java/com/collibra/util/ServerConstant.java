package com.collibra.util;

public interface ServerConstant {

    int PORT = 50000;
    int WAITING_TIME = 30;

    String PHASE1_DEFAULT_MESSEGE  = "SORRY, I DID NOT UNDERSTAND THAT";
    String PHASE1_PREFIX = "HI, I AM ";
    String PHASE1_SUFFIX = "BYE MATE!";
    String PHASE2_ADD_NODE = "ADD NODE";
    String PHASE2_ADD_EDGE = "ADD EDGE";
    String PHASE2_REMOVE_NODE = "REMOVE NODE";
    String PHASE2_REMOVE_EDGE = "REMOVE EDGE";
    String PHASE3_PREFIX = "SHORTEST PATH";
    String PHASE4_PREFIX = "CLOSER THAN";

}
